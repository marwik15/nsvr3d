#include <BGIm/graphics.h>
#include <dos.h>
#include <time.h>
#include <iostream>

#include "engine.h"
#include "core/platform.h"
#include "scene/Scene.h"
#include "core/math.h"
#include "core/file/fileHelper.h"

#define GLM_FORCE_MESSAGES
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <stdio.h>

namespace svr = SVR;

using namespace std;
using namespace SVR;


class NSVR3D : public svr::Engine, public svr::Input, public SVR::Data{

private:
	Scene scene;
	
	Object3Ddata d;
	std::vector<Vector3Df> objectV;
	std::vector<Vector2D> perspectivePoints;

	glm::tvec4<float> data;

	glm::mat4  model = glm::mat4(1.0f);
	glm::mat4 view = glm::mat4(1.0f);

	glm::mat4 projection;

	glm::vec3 cameraPos = glm::vec3(0.0f, 0.0f, 0.0);
	glm::vec3 cameraTarget = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::vec3 cameraDirection = glm::normalize(cameraPos - cameraTarget);
	glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
	glm::vec3 cameraUp = glm::vec3(0.0f, 100.0f, 0.0f);
	float cameraSpeed = 100.05f;

	float angle = 20.0f;
	float radius = 0.0f;

	bool firstMouse = true;
	float yaw = -90.0f;
	float pitch = 0.0f;
	float lastX = 800.0f / 2.0;
	float lastY = 600.0 / 2.0;
	float fov = 45.0f;

	void onCreate() override {
		std::cout << R"("new Software Vector Renderer 3D" has started)"<<std::endl;
		
		scene.create("mainScene");
		scene.insertObject(loadFile(loadFileDialog()));

		scene.getFirstObject(d);
		
		objectV = d.vertex3D;
		
		projection = glm::perspective(90.0f, (float)800 / 600, 0.1f, 1000.0f);
		
	}
	
	void mouseCallBack(double xpos, double ypos) {
		if (firstMouse){
			lastX = xpos;
			lastY = ypos;
			firstMouse = false;
		}

		float xoffset = xpos - lastX;
		float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top
		lastX = xpos;
		lastY = ypos;

		float sensitivity = 0.1f; // change this value to your liking
		xoffset *= sensitivity;
		yoffset *= sensitivity;

		yaw += xoffset;
		pitch += yoffset;

		// make sure that when pitch is out of bounds, screen doesn't get flipped
		if (pitch > 89.0f)
			pitch = 89.0f;
		if (pitch < -89.0f)
			pitch = -89.0f;

		glm::vec3 front;
		front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
		front.y = sin(glm::radians(pitch));
		front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
		cameraFront = glm::normalize(front);
	}


	void keyCallback(svr::Timestep ts) {

		char key = readKeyboard();

		if (key == 'w')
			cameraPos += cameraSpeed * cameraFront;
		if (key == 's')
			cameraPos -= cameraSpeed * cameraFront;
		if (key == 'a')
			cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
		if (key == 'd')
			cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;

		if (key == KEY_UP) 
			cameraPos += glm::vec3(0.0f, 30.0f*ts, 0.0) * cameraSpeed;
		if (key == KEY_DOWN)
			cameraPos -= glm::vec3(0.0f, 30.0f*ts, 0.0) * cameraSpeed;

		if (key == 27) //esc
			exit(1);

		key = ' ';
	}

	void OnUpdate(svr::Timestep ts) override {

		keyCallback(ts);

		POINT p;
		GetCursorPos(&p);
		
		ShowCursor(FALSE);
		mouseCallBack(p.x, p.y);

		radius += 150.0f * ts;
		
		view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);

		model = glm::mat4(1.0f);
		float scaleFactor = 500.0f;
		//model = glm::translate(model, glm::vec3(angle, 0.0f, 0.0f));
		model = glm::rotate(model, glm::radians(angle), glm::vec3(1.0f, 1.0f, 0.0f));
		//model = glm::scale(model, glm::vec3(scaleFactor));
		angle += 100.3f*ts;
		perspectivePoints.clear();
		for (auto r : objectV) {
			
		
			glm::vec4 result = glm::vec4(1.0f);
		
			data = glm::tvec4<float>(r.x, r.y, r.z, 1);
		
			result = projection * view * model * data;
		
			svr::Vector2D v = svr::perspective(Vector3Df(result.x, result.y, result.z), 1000);
			setcolor(RED);
			putpixel(v.x, v.y, WHITE);
			perspectivePoints.push_back(v);
			//circle(v.x, v.y, 3);
			
		}


		// f v/vt/vn v/vt/vn v/vt/vn v/vt/vn

		int poly[10];
		int maxx, maxy;

		maxx = getmaxx();

		maxy = getmaxy();
		
		for (auto p : face) {
			int re = p.f1.x - 1;

			poly[0] = perspectivePoints[p.f1.x - 1].x;
			poly[1] = perspectivePoints[p.f1.x - 1].y;
			
			poly[2] = perspectivePoints[p.f2.x - 1].x;
			poly[3] = perspectivePoints[p.f2.x - 1].y;

			poly[4] = perspectivePoints[p.f3.x - 1].x;     
			poly[5] = perspectivePoints[p.f3.x - 1].y;
			
			poly[6] = perspectivePoints[p.f4.x - 1].x;  
			poly[7] = perspectivePoints[p.f4.x - 1].y;      

			fillpoly(4, poly);  
		}
	
		swapBuffor();
		
	}

};


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	
	svr::Plaftorm console;
	console.createConsole();

	NSVR3D nSVR3D;
	nSVR3D.Start();
	
	return 1;

}
