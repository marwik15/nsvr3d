#pragma once
#include "core/platform.h"
#include "core/draw.h"
#include <iostream>
#include "core/file/fileParser.h"
#include "core/Timestemp.h"

namespace SVR {

	class Engine : public Plaftorm, public SVR::fileParser, public SVR::Draw {
	public:
		virtual void onCreate() {}
		virtual void OnUpdate(Timestep ts) {}

		void Start() {
			initwindow(800, 600, "new Software Vector Renderer 3D");
			onCreate();
			loop();
		}

		void swapBuffor() {
			swapbuffers();
			cleardevice();
		}

	
	private:

		void loop() {
			StartCounter();
			double FPSc = 0;
			float FPS = 0;
			double fpsc = 0;

			while (1) {
				float time = GetCounter();
				Timestep timestep = time - m_lastFrameTime;

				m_lastFrameTime = time;
				
				OnUpdate(timestep);
				
				fpsc += timestep;
				FPSc++;
				//count frames
				if (fpsc >= 1.0) {
					FPS = FPSc;
				}

				//update frame counter
				if (fpsc >= 1.0) {
					fpsc = FPSc = 0;
				}
				
				//gotoxy(1, 7);
				//std::cout << "frame time (ms) "  << timestep.getMiliseconds() <<"    ";
				//gotoxy(1, 8);
				//std::cout << "FPS " << FPS << "    ";

				std::string s = std::to_string((int)FPS);
				setcolor(WHITE);
				outtextxy(750, 0, (char*)s.c_str());

			}
		}

	};
}

