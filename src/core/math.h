#pragma once
#include <math.h>
#include <vector>

namespace SVR {


	struct Vector2D
	{
		Vector2D(){}
		Vector2D(int x, int y) { this->x = x; this->y = y; }
		int x=0, y=0;

	};

	struct Vector3Df {
		Vector3Df():x(0),y(0),z(0){}
		Vector3Df(float x, float y, float z) { this->x = x, this->y = y, this->z = z; }
		float x, y, z;

	};

	struct Vector4Df {
		Vector4Df() :x(0), y(0), z(0),w(0) {}
		Vector4Df(float x, float y, float z, float w) { this->x = x, this->y = y, this->z = z, this->w = w; }
		float x, y, z,w;
	};
 

	Vector2D perspective(Vector3Df vertex, float d) {

		const float screenX = 800.0;
		const float screenY = 600.0;

		Vector2D V;

		V.x = (vertex.x * d) / (vertex.z + d) + screenX / 2;
		V.y = screenY / 2 - (vertex.y * d) / (vertex.z + d);

		return V;

	}

	std::vector<Vector3Df> scale(std::vector<Vector3Df> vec,int scaleFactor) {
		std::vector<Vector3Df> vRet;
		Vector3Df v;

		for (auto v : vec) {
			v.x = v.x * scaleFactor;
			v.y = v.y * scaleFactor;
			v.z = v.z * scaleFactor;
			vRet.push_back(v);
		}
		return vRet;
	}


	std::vector<Vector3Df> scale(std::vector<Vector3Df> vec, float x, float y, float z) {
		Vector3Df v;

		std::vector<Vector3Df> vRet;
		for (auto v : vec) {
			v.x *= x;
			v.y *= y;
			v.z *= z;
			vRet.push_back(v);

		}

		return vRet;
	}

	

	std::vector<Vector3Df> move(std::vector<Vector3Df> vec, double x, double y, double z) {
		Vector3Df v;

		std::vector<Vector3Df> vRet;

		for (auto v : vec) {

			v.x =  v.x + x;
			v.y += y;
			v.z += z;
			vRet.push_back(v);
		}
		return vRet;
	}

	std::vector<Vector3Df> rotate(std::vector<Vector3Df> vec, double angle, double phi){

			std::vector<Vector3Df> vRet;

			for (auto v : vec) {

				v.x = sin(angle) * cos(phi);
				v.y = sin(angle) * sin(phi);
				v.z = cos(angle);

				vRet.push_back(v);
			}

			
			return vRet;
		}

	

	std::vector<Vector3Df> rotate(std::vector<Vector3Df> vec,double alphaX, double alphaY, double alphaZ) {

		double x0, y0, z0;

		

		std::vector<Vector3Df> vRet;
		
		for (auto v : vec) {
			
			x0 = v.z * sin(alphaY) + v.x * cos(alphaY);
			y0 = v.y;
			z0 = v.z * cos(alphaY) - v.x * sin(alphaY);

			v.x = x0;
			v.y = y0;
			v.z = z0;

			x0 = v.x;
			y0 = v.z * sin(alphaX) + v.y * cos(alphaX);
			z0 = v.z * cos(alphaX) - v.y * sin(alphaX);

			v.x = x0;
			v.y = y0;
			v.z = z0;

			x0 = v.y * sin(alphaZ) + v.x * cos(alphaZ);
			y0 = v.y * cos(alphaZ) - v.x * sin(alphaZ);
			z0 = v.z;

			/*x0 = v.y*sin(alpha) + v.x*cos(alpha);
			y0 = v.y*cos(alpha) - v.z*sin(alpha);
			z0 = v.z;*/

			//std::cout << v.x << " " << v.y << " " << v.z << std::endl;
			
			v.x = x0;
			v.y = y0;
			v.z = z0;
			vRet.push_back(v);

		}
		return vRet;
	}

}