#pragma once
#include <iostream>
#include "math.h"
#include <fstream>
#include <string>
#include <vector>
#include "core/file/generalParser.h"
#include "data/data.h"

namespace SVR {

	class ObjFile : public SVR::generalParse,public Data{
	public:

		SVR::Data::Object3Ddata parse(std::vector<std::string> loadedFile) override {
			///wip
			//validateFile(loadedFile);
			
			SVR::Data::Object3Ddata obj;

			std::cout << ".obj parsing...\n";
			for (auto d : loadedFile) {
				parseLine(d);
			}
			std::cout << ".obj parsing finished successfully \n";

			obj.objectName = "generic name";
			obj.vertex3D = vertex;
			
			return obj;
		}

		
		std::vector<Face4> face;
	private:
		std::vector<SVR::Vector3Df> vertex;
		std::vector<std::string> oName;
		std::vector<SVR::Vector3Df> tCord;
		std::vector<SVR::Vector3Df> vNormal;

		struct FoundData {
			std::string blackListed;
			std::string debugOldLine;
			std::string debugNewLine;
			int lineNumber;
			int pos;

		};

		void parseLine(std::string line)override{

			if (line[0] == 'v' && line[1] == ' ') {
				Vector3Df Vvec;
				float x, y, z;
				char v;
				int re = sscanf(line.c_str(), "%c %f %f %f",&v, &x, &y, &z);
				Vvec.x = x;
				Vvec.y = y;
				Vvec.z = z;

				vertex.push_back(Vvec);

			}

			if (line[0] == 'f' && line[1] == ' ') {
				float a, b, c, d, e, f, g, h, i, j, k, l;
				char v;
				int re = sscanf(line.c_str(), "%c %f/%f/%f %f/%f/%f %f/%f/%f %f/%f/%f", &v, &a, &b, &c, &d, &e, &f, &g, &h, &i, &j, &k, &l);

				face.push_back(Face4(a, b, c, d, e, f, g, h, i, j, k, l));
			}

			if (line[0] == 'v' && line[1] == 'n') {

				char v;
				float x, y, z;
				int re = sscanf(line.c_str(), "%c %f %f %f", &v, &x, &y, &z);

				vNormal.push_back(Vector3Df(x, y, z));
			}

			if (line[0] == 'v' && line[1] == 't') {}
			if (line[0] == 'o' && line[1] == ' ') {}

		}

		std::string setSupportedFileType()override {
			return "obj";
		}

		void validateFile(std::vector<std::string> &file) {
			std::cout << "validating .obj\n";
			size_t pos = 0;

			std::vector <std::string> blackList = { "//" };

			for (int i = 0; i < file.size();i++) {
				
				std::vector<FoundData> found;
				pos = std::string::npos;

				if (file[i][0] == 'f') {

					for (auto b : blackList) {

						FoundData foundData;
						foundData.blackListed = b;

						pos = file[i].find(b, 0);

						while (pos != std::string::npos){
							foundData.debugOldLine = file[i];

							foundData.lineNumber = i;
							foundData.pos = pos;

							pos = file[i].find(b, pos + 1);

							
							file[i].replace(5, pos,"/");
							foundData.debugNewLine = file[i];
							found.push_back(foundData);
						}

					}

					//std::cout << "Old line : " << oldLine
					//	<< "\nnew line " << file[i] << std::endl;

					for (auto re : found) {
						std::cout << "found: " << re.blackListed << " line number " << re.lineNumber << " pos " << re.pos << std::endl
							<<"debug old line "<< re.debugOldLine << std::endl <<"debug new line "<< re.debugNewLine << std::endl << std::endl;;

					}
				}

			}
		}

	};
}