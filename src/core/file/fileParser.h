#pragma once
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <list>

#include "core/file/fileHelper.h"
#include "core/math.h"
#include "core/file/objParser.h"

#include "data/data.h"


namespace SVR {

	class fileParser : public SVR::ObjFile {
	
	public:
		std::vector<Vector3Df> vertexX;

	private:
		double long verticesCount, normalsCount, faceCount;
		std::list<std::string> supportedFileFormats = { ";" };

		std::vector<std::string> m_loadedFile;

		SVR::fileHelper m_fileHelper;


	public:
		fileParser() {
			verticesCount = normalsCount = faceCount = 0;;
		}
		
		SVR::Data::Object3Ddata loadFile(std::string path) {
			m_fileHelper.setPath(path);
			m_fileHelper.extractExtention(path);

			openAndloadFileToMemory(path);

			return parse(m_loadedFile);


		}

		void printSupportedFileFormats() {
			for (auto re : supportedFileFormats) {

				std::cout << re << std::endl;
			}
		}


		void openAndloadFileToMemory(std::string path) {
			std::cout << "attempt to load the file\n";
		
			std::ifstream file(path, std::ios::in);
			std::string line;

			if (file.is_open()) {
				std::cout << "file loaded successfully\n";
				while (getline(file, line))
				{
					//std::cout << line << '\n';
					m_loadedFile.push_back(line);
				}
				file.close();
			}
			
			else {
				printf("Unable to open this file !\n");
				file.close();
				return;
			}

		}

		std::vector<std::string> getLoadedFile() {
			return m_loadedFile;
		}

	};
}