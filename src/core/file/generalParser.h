#pragma once

#include <iostream>
#include "data/data.h"
namespace SVR {
	class generalParse {

	public:

		virtual void parseLine(std::string line) {}

		virtual std::string setSupportedFileType() { return ""; }
		virtual SVR::Data::Object3Ddata parse(std::vector<std::string> loadedFile) {SVR::Data::Object3Ddata re;return re;}

	private:
		std::string fileType;

	};

}
