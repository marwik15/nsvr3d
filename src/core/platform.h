#pragma once
#include <BGIm/graphics.h>
#include "input/input.h"
#include <windows.h>
#include <string>

namespace SVR {

	class Plaftorm {
	private:

		double PCFreq = 0.0;
		__int64 CounterStart = 0;

	public:
		
		void gotoxy(int x, int y){
			COORD c;
			c.X = x - 1;
			c.Y = y - 1;
			SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
		}

		float m_lastFrameTime = 0.0f; //temporary public
		void StartCounter(){
			LARGE_INTEGER li;
			if (!QueryPerformanceFrequency(&li))
				std::cout << "QueryPerformanceFrequency failed!\n";

			PCFreq = double(li.QuadPart);

			QueryPerformanceCounter(&li);
			CounterStart = li.QuadPart;
		}
		double GetCounter(){
			LARGE_INTEGER li;
			QueryPerformanceCounter(&li);
			return double(li.QuadPart - CounterStart) / PCFreq;
		}

		void showConsole() {
			AllocConsole();
		}
		void hideConsole() {
			FreeConsole();
		}
		void createConsole() {
			resetConsole();
			showConsole();
		}

		void resetConsole() {
			//from https://stackoverflow.com/a/57241985
			if (!AllocConsole()) {
				// Add some error handling here.
				// You can call GetLastError() to get more info about the error.
				return;
			}

			// std::cout, std::clog, std::cerr, std::cin
			FILE* fDummy;
			freopen_s(&fDummy, "CONOUT$", "w", stdout);
			freopen_s(&fDummy, "CONOUT$", "w", stderr);
			freopen_s(&fDummy, "CONIN$", "r", stdin);
			std::cout.clear();
			std::clog.clear();
			std::cerr.clear();
			std::cin.clear();

			// std::wcout, std::wclog, std::wcerr, std::wcin
			HANDLE hConOut = CreateFile(("CONOUT$"), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
			HANDLE hConIn = CreateFile(("CONIN$"), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
			SetStdHandle(STD_OUTPUT_HANDLE, hConOut);
			SetStdHandle(STD_ERROR_HANDLE, hConOut);
			SetStdHandle(STD_INPUT_HANDLE, hConIn);
			std::wcout.clear();
			std::wclog.clear();
			std::wcerr.clear();
			std::wcin.clear();
		}

		std::string loadFileDialog() {
			OPENFILENAME ofn;
			char filePath[MAX_PATH] = "";

			ZeroMemory(&ofn, sizeof(ofn));
			ofn.lStructSize = sizeof(ofn);
			ofn.hwndOwner = NULL;
			//ofn.lpstrFilter = "obj wavefront (*.obj)\0*.obj\0All Files (*.*)\0*.*\0";
			ofn.lpstrFilter = "obj wavefront (*.obj)\0*.obj\0";
			ofn.lpstrFile = filePath;
			ofn.nMaxFile = MAX_PATH;
			ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_HIDEREADONLY | OFN_ALLOWMULTISELECT;
			ofn.lpstrDefExt = "obj";

			if (!GetOpenFileName(&ofn))
			{
				std::cout << "file error !\n";
				return "";
			}

			return filePath;
		}
	
	private:
		

	};
}
