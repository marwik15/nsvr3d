#pragma once
#include <string>
#include <vector>
#include <iostream>

#include "data/data.h"

class Scene : public SVR::Data{
public:
	Scene() {
		sceneName.clear();
	}
	void create(std::string name) {
		sceneName = name;
	}

	void insertObject(Object3Ddata object) {
		sceneObjects.push_back(object);
		
	}

	void objectList(){}
	void objectCount(){}

	void getFirstObject(Object3Ddata &data) {
		data = sceneObjects.front();
		
	}

	std::string getSceneName() const{
		return sceneName;
	}

private:
	
	std::vector<Object3Ddata> sceneObjects;
	std::string sceneName;

};