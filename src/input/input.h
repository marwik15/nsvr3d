#pragma once
#include <BGIm/graphics.h>
#include "keyboardInput.h"
#include "mouseInput.h"

namespace SVR {

	class Input : public keyboardInput, public mouseInput {
	public:

		void readUserInput() {
			if (mouseEvent(LBUTTONUP)) {
				std::cout << "LBUTTONUP\n";
			}
			if (mouseEvent(LBUTTONDOWN)) {
				std::cout << "LBUTTONDOWN\n";
			}

			readKeyboard();

		}

	};
}