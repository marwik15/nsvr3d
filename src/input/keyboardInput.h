#pragma once
#include <BGIm/graphics.h>
#include <iostream>
#include "core/platform.h"
#include <vector>

namespace SVR {

	class keyboardInput  {
	private:
		char key = 0;
	public:
		keyboardInput() {
			m_inputChar = ' ';
		}
		

		char readKeyboard() {
			key = ' ';
			if (kbhit()) {
				m_inputChar = getch();
				return m_inputChar;
			}
			return key;
		}

	private:
		char m_inputChar;


	};
}