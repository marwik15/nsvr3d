#pragma once
#include <BGIm/graphics.h>

namespace mouseEvents {
	#define	MOUSEMOVE		WM_MOUSEMOVE
	#define	LBUTTONDBLCLK   WM_LBUTTONDBLCLK
	#define	LBUTTONDOWN		WM_LBUTTONDOWN
	#define	LBUTTONUP	    WM_LBUTTONUP
	#define	MBUTTONDBLCLK	WM_MBUTTONDBLCLK
	#define	MBUTTONDOWN	    WM_MBUTTONDOWN
	#define	MBUTTONUP	    WM_MBUTTONUP
	#define	RBUTTONDBLCLK	WM_RBUTTONDBLCLK
	#define	RBUTTONDOWN  	WM_RBUTTONDOWN
	#define	RBUTTONUP	    WM_RBUTTONUP
}

class mouseInput {

public:

	bool mouseEvent(int kind) {
		if (ismouseclick(kind)) {
			clearmouseclick(kind);
			return true;
		}
		return false;
	}

private:
	void readMouseXY() {
		this->m_mouseX = mousex();
		this->m_mouseY = mousey();
	}
	
	int getMouseX() {
		return m_mouseX;
	}

	int getMouseY() {
		return m_mouseY;
	}

private:
	int m_mouseX;
	int m_mouseY;

};