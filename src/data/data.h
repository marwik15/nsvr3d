#pragma once
#include <string>
#include <vector>
#include <core/math.h>

namespace SVR {

	class Data  {
	public:
		struct Object3Ddata {
			std::string objectName;
			std::vector<SVR::Vector3Df> vertex3D;
			std::vector<SVR::Vector3Df> vertex3DNormal;
			std::vector<SVR::Vector3Df> vertex3DUV;

		};

		struct Face3 {
			Vector3Df f1;
			Vector3Df f2;
			Vector3Df f3;
		};

		struct Face4 {
			Face4(float a, float b, float c, float d, float e, float f, float g, float h, float i, float j, float k, float l) {
				f1.x = a;
				f1.y = b;
				f1.z = c;

				f2.x = d;
				f2.y = e;
				f2.z = f;

				f3.x = g;
				f3.y = h;
				f3.z = i;
				
				f4.x = j;
				f4.y = k;
				f4.z = l;
				
			}
			Vector3Df f1;
			Vector3Df f2;
			Vector3Df f3;
			Vector3Df f4;
		};


	};

}
